﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data as IEnumerable<T>);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task CreateAsync(T entity)
        {
            var isExist = Data.Any(ent => ent.Id == entity.Id);
            if (isExist)
            {
                throw new NotFoundException(nameof(entity), entity.Id);
            }
            Data.Add(entity);
            return Task.CompletedTask;
        }

        public Task UpdateAsync(T entity)
        {
            var existEntity = Data.FirstOrDefault(ent => ent.Id == entity.Id);
            var isDelete = Data.Remove(existEntity);
            if (!isDelete)
            {
                throw new NotFoundException(nameof(entity), entity.Id);
            }
            Data.Add(entity);
            return Task.CompletedTask;
        }

        public Task DeleteAsync(Guid entityId)
        {
            var entity = Data.FirstOrDefault(ent => ent.Id == entityId);
            if (entity is null)
            {
                throw new NotFoundException(nameof(entity), entityId);
            }
            Data.Remove(entity);
            return Task.CompletedTask;
        }
    }
}